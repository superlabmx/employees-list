<?php namespace Superlabmx\Employees\Components;

use Lang;
use Cms\Classes\ComponentBase;
use Rainlab\User\Models\User as UserModel;

class EmployeeList extends ComponentBase
{

    public function componentDetails()
    {
      return [
        'name'        => 'superlabmx.employees::lang.components.userlist.name',
        'description' => 'superlabmx.employees::lang.components.userlist.description'
      ];
    }

    public function defineProperties()
    {
      return [
        'templateLayout' => [
          'type'        => 'dropdown',
          'default'     => 'blank',
          'options'     => ['circles' => 'Circles', 'cards' => 'Cards', 'blank' => 'Blank'],
          'title'       => 'superlabmx.employees::lang.components.userlist.properties.templateLayout.title',
          'description' => 'superlabmx.employees::lang.components.userlist.properties.templateLayout.description',

        ],
        'maxItems' => [
          'default'           => '2',
          'type'              => 'string',
          'validationPattern' => '\b([1-9]|[0-9][0-2])\b',
          'title'             => 'superlabmx.employees::lang.components.userlist.properties.maxItems.title',
          'description'       => 'superlabmx.employees::lang.components.userlist.properties.maxItems.description',
          'validationMessage' => Lang::get('superlabmx.employees::lang.components.userlist.properties.maxItems.validationMessage')
        ]
      ];
    }

    public function onRun(){
      //Set the layout template to load in the page
      $this->page['templateLayout'] = $this->property('templateLayout');
      //Search for all the users that are part of the employee group
      $users = UserModel::whereHas('groups',function($query){
        $query->where('code', 'employee');
      })->take($this->property('maxItems'))->get();
      //Set the Users variable in the page
      $this->page['employeeList'] = array('employees' => $users);
    }
}
