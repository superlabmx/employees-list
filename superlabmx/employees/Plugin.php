<?php namespace Superlabmx\Employees;

use System\Classes\PluginBase;
use Input;
use Redirect;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UserController;
use Superlabmx\Employees\Models\Employee;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function registerComponents()
    {
      return [
        'Superlabmx\Employees\Components\EmployeeList' => 'employeeList'
      ];
    }

    public function registerSettings()
    {
    }

    public function pluginDetails()
    {
        return [
            'name'        => 'superlabmx.employees::lang.plugin.name',
            'description' => 'superlabmx.employees::lang.plugin.description',
            'author'      => 'Superlabmx',
            'icon'        => 'icon-users',
            'homepage' => 'http://www.superlab.mx'
        ];
    }

    public function boot(){
      UserModel::extend(function($model){
        $model->hasOne['employee_profile'] = ['Superlabmx\Employees\Models\Employee'];
      });
      //Extend Form Fields for RainLab User
      UserController::extendFormFields(function($form, $model, $context){
        //Check if the Model is an instance of the User Model and if exists
        if(!$model instanceof UserModel || !$model->exists)
          return;
        //Check if the User belongs to the Employee group
        if(!$model->groups->where('code','employee')->first())
          return;
        //Try to get The Employee profile
        Employee::getProfileFromUser($model);
        //Add the TabFields to the users Controller
        $form->addTabFields([
          'employee_profile[screen_name]' => [
            'span' => 'left',
            'label' => 'superlabmx.employees::lang.tab.screen_name',
            'tab' => 'superlabmx.employees::lang.tab.name',
          ],
          'employee_profile[position]' => [
            'span' => 'left',
            'label' => 'superlabmx.employees::lang.tab.position',
            'tab' => 'superlabmx.employees::lang.tab.name'
          ],
          'employee_profile[employee_number]' => [
            'span' => 'right',
            'label' => 'superlabmx.employees::lang.tab.employee_number',
            'tab' => 'superlabmx.employees::lang.tab.name',
          ],
          'employee_profile[biography]' => [
            'size' => 'small',
            'type' => 'textarea',
            'label' => 'superlabmx.employees::lang.tab.biography',
            'tab' => 'superlabmx.employees::lang.tab.name'
          ],
          'employee_profile_social_network_section' => [
            'label' => 'superlabmx.employees::lang.tab.socialnetwork',
            'type' => 'section',
            'tab' => 'superlabmx.employees::lang.tab.name'
          ],
          'employee_profile[facebook_url]' => [
            'span' => 'auto',
            'label' => 'superlabmx.employees::lang.tab.facebook_url',
            'tab' => 'superlabmx.employees::lang.tab.name'
          ],
          'employee_profile[twitter_url]' => [
            'span' => 'auto',
            'label' => 'superlabmx.employees::lang.tab.twitter_url',
            'tab' => 'superlabmx.employees::lang.tab.name'
          ],
          'employee_profile[google_plus_url]' => [
            'span' => 'auto',
            'label' => 'superlabmx.employees::lang.tab.google_plus_url',
            'tab' => 'superlabmx.employees::lang.tab.name'
          ],
        ]);
      });
    }
}
