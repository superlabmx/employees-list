<?php return [
    'plugin' => [
        'name' => 'User Employees',
        'description' => 'Plugin that allows display a list of employees at the front-end and administer them in the backend using groups.'
    ],
    'tab' => [
      'name' => 'Employee Data',
      'screen_name' => 'Employee Shortname',
      'socialnetwork' => 'Social Networks',
      'employee_number' => 'Employee Number',
      'position' => 'Employee Position',
      'biography' => 'Short Biography',
      'facebook_url' => 'Facebook Account',
      'twitter_url' => 'Twitter Account',
      'google_plus_url' => 'Google+ Account'
    ],
    'components' => [
      'userlist' => [
        'name' => 'UserList',
        'description' => 'A useful component to add a user list based on a group and a template.',
        'properties' => [
          'templateLayout' => [
            'title'       => 'Layout del Template',
            'description' => 'Seleccione uno de los templates disponibles para desplegar la lista de usuarios.'
          ],
          'maxItems' => [
            'title'             => 'Número de Empleados',
            'description'       => 'Seleccione un número de empleados a desplegar en su lista (máximo 12 empleados).',
            'validationMessage' => 'Please select a number of employees between 1 and 12'
          ]
        ]
      ]
    ]
];
