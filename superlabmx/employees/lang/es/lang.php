<?php return [
    'plugin' => [
        'name' => 'User Employees',
        'description' => 'Plugin que permite desplegar la lista de empleados en el front end y ad ministrarlos en el backend mediante grupos.'
    ],
    'tab' => [
      'name' => 'Datos de Empleado',
      'screen_name' => 'Nombre Corto del Empleado',
      'socialnetwork' => 'Redes Sociales',
      'employee_number' => 'Número del Empleado',
      'position' => 'Posición dentro de la Empresa',
      'biography' => 'Biografía Corta',
      'facebook_url' => 'Usuario de Facebook',
      'twitter_url' => 'Usuario de Twitter',
      'google_plus_url' => 'Usuario de Google+'
    ],
    'components' => [
      'userlist' => [
        'name' => 'UserList',
        'description' => 'Un componente útil para añadir una lista de usuarios basada en un grupo y una plantilla.',
        'properties' => [
          'templateLayout' => [
            'title'       => 'Layout del Template',
            'description' => 'Seleccione uno de los templates disponibles para desplegar la lista de usuarios.'
          ],
          'maxItems' => [
            'title'             => 'Número de Empleados',
            'description'       => 'Seleccione un número de empleados a desplegar en su lista (máximo 12 empleados).',
            'validationMessage' => 'Seleccione un número valido de empleados entre 1 y 12.'
          ]
        ]
      ],
    ]
];
