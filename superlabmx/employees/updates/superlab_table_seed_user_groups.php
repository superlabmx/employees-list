<?php namespace Superlabmx\Employees\Updates;

use Seeder;
use RainLab\Users\Models\UserGroup;

class SeedUsersTable extends Seeder
{
    public function run()
    {
        if(!UserGroup::where('code','employee')->first()){
          UserGroup::create([
              'name'                  => 'Employee',
              'code'                  => 'employee',
              'description'           => 'Users that part of the Team and works for the company'
          ]);
        }
    }
}
