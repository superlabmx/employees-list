<?php namespace Superlabmx\Employees\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSuperlabmxEmployeesProfile extends Migration
{
    public function up()
    {
        Schema::create('superlabmx_employees_profile', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->text('screen_name')->nullable();
            $table->integer('employee_number')->nullable();
            $table->text('position')->nullable();
            $table->text('biography')->nullable();
            $table->text('facebook_url')->nullable();
            $table->text('twitter_url')->nullable();
            $table->text('google_plus_url')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('superlabmx_employees_profile');
    }
}
