<?php namespace Superlabmx\Employees\Models;

use Model;
/**
 * Model
 */
class Employee extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $appends = ['avatar','contact_methods'];
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'superlabmx_employees_profile';

    public $belongsTo = [
      'user' => ['RainLab\User\Models\User']
    ];

    public static function getProfileFromUser($user){
      //Search for the Employee Profile
      if($user->employee_profile){
        return $user->employee_profile;
      }
      //Create an employee profile for this user
      $employee_profile = new static;
      $employee_profile->screen_name = $user->name.' '.$user->surname;
      $employee_profile->user = $user;
      $employee_profile->save();
      //Reverse back the User relation
      $user->employee_profile = $employee_profile;
      //Return the Employee Profile
      return $employee_profile;
    }

    public function getAvatarAttribute(){
      $avatar = null;
      if($this->user->avatar && file_exists($this->user->avatar->getLocalPath())){
        $avatar = $this->user->avatar->getThumb(350, 350, ['mode' => 'crop']);
      }
      return $avatar;
    }

    public function getContactMethodsAttribute(){
      $contact_methods = array();
      //Add the email of the user as a contact method
      $contact_methods['mail'] = array(
        'icon' => 'envelope',
        'link' => 'mailto:'.$this->user->email,
      );
      if($this->facebook_url){
        $contact_methods['facebook'] = array(
          'icon' => 'facebook',
          'link' => 'http://facebook.com/'.$this->facebook_url,
        );
      }
      if($this->twitter_url){
        $contact_methods['twitter'] = array(
          'icon' => 'twitter',
          'link' => 'http://twitter.com/'.$this->twitter_url,
        );
      }
      return $contact_methods;
    }

}
